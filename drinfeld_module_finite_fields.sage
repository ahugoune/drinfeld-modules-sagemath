import itertools
import logging
import operator
import random
import string

# Import does not work for Sage files
load('ore_polynomials_utils.sage')

# For typing
from sage.rings.finite_rings.element_givaro import FiniteField_givaroElement
from sage.rings.finite_rings.finite_field_base import FiniteField
from sage.rings.finite_rings.integer_mod import IntegerMod_int
from sage.rings.integer import Integer
from sage.rings.morphism import RingHomomorphism
from sage.rings.polynomial.ore_polynomial_element import OrePolynomial
from sage.rings.polynomial.ore_polynomial_ring import OrePolynomialRing
from sage.rings.polynomial.polynomial_element import Polynomial
from sage.rings.polynomial.polynomial_ring import PolynomialRing_dense_finite_field

FORMAT = '%(levelname)s %(funcName)s: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)


class DrinfeldModule_finite_fields():
    """
    A class for Drinfeld modules in finite fields.
    """

    # Note: in methods which take a polynomial `a` (in Fq[X]) as an
    # argument, there can be errors when we try to access the
    # coefficients of a via a.dict() if a is a scalar. Right now, the
    # workaround is to cast a to self.__A(a) in self.image method.
    # Further investigation and a better workaround may be useful.

    def __init__(self, A: PolynomialRing_dense_finite_field,
                 Ktau: OrePolynomialRing, phi_X: OrePolynomial):
        # Check everything
        Fq = A.base_ring()
        K = phi_X.base_ring()
        assert Fq.characteristic() == K.characteristic()
        assert Fq.degree().divides(K.degree())
        assert phi_X in Ktau
        assert not phi_X.is_constant()
        # This is useful for method lying_polynomial
        KT.<T> = K[]
        self.__KT = KT
        # Save data
        self.__A = A
        self.__Ktau = Ktau
        self.__phi_X = phi_X

    # ┌───────────────┐
    # │ Class methods │
    # └───────────────┘

    @classmethod
    def r2_from_j_invariant(cls, A, Ktau, j):
        """
        Construct a Drinfeld module A -> Ktau with j-invariant `j`.
        The j-invariant of such a Drinfeld module given by the image of
        X `gamma(X) + g*t + Delta*t^2` equals `g^(q+1))/Delta`, so we
        pick g=1 and Delta=j^{-1}.
        """
        K = Ktau.base_ring()
        t = Ktau.gen()
        phi_X = K.gen() + t + (j^(-1))*t^2
        return cls(A, Ktau, phi_X)

    @classmethod
    def random(cls, A, Ktau, rank):
        """
        Return a random DrinfeldModule_finite_fields A -> Ktau with rank
        `rank`.
        """
        K = Ktau.base_ring()
        t = Ktau.gen()
        phi_X = Ktau(K.gen())
        for n in range(1 , rank):
            phi_X += K.random_element() * (t^n)
        phi_X_rank = 0
        while phi_X_rank == 0:
            phi_X_rank = K.random_element()
        phi_X += phi_X_rank * (t^rank)
        return cls(A, Ktau, phi_X)

    # ┌─────────┐
    # │ Methods │
    # └─────────┘

    def all_onedim_torsion_subspaces_gens(self, m):
        """
        Return a generator of all 1-dim sub-A/m-vect. subspaces of the
        m-torsion (so, this excludes Vect(O)), as well as a couple (D,
        f), where D is the extension of self.K in which those elements
        live and f: K -> D is the canonical injection.
        """
        # TODO: Verifications
        assert m.is_prime()
        D, f, torsion_gen = self.torsion(m)
        partial_torsion = set(filter(lambda x: x != 0, torsion_gen()))
        gens = []  # List of generators
        while partial_torsion:
            # Get an element, add it to the list and eliminate all
            # elements it generates
            V_gen = partial_torsion.pop()
            gens.append(V_gen)
            V = self.onedim_torsion_subspace(m, V_gen, (D, f))
            partial_torsion -= set(V)
        return (gens, (D, f))

    def are_independent(self, m, t1, t2, Omega=None, return_pol=False):
        """
        Given a prime polynomial m in A, an element a of the quotient
        A/<m> and two m-torsion elements t1, t2 in the algebraic closure
        of K, together with an algebra (optional) Omega=(M, f) such that
        M is an algebraic extension of K containing t1 and t2 and f the
        injection from K to L, tell if t1 and t2 are linearly
        independent for the A/<m>-vector-space law induced by self.
        TODO: Tell return format.

        This is done by solving a linear system.
        TODO: Explain.
        """
        # TODO: Verifications
        M, f = self.__give_algebra(Omega)
        M_vs, from_M_vs, to_M_vs = M.free_module(self.Fq, map=True)
        deg = m.degree()
        # This case is not handled by the thereafter linear system
        if t1 == 0:
            return (False, None) if return_pol else False
        # Create matrix and vector
        #      list of vectors (phi_X^i(t1)[0] … phi_X^i(t1)[n-1])
        cols = [to_M_vs(self.module_law(self.X^i, t1, Omega)) for i in range(deg)]
        Mat = Matrix(cols).transpose()
        B = to_M_vs(t2)
        # Solve this shit
        try:
            A = Mat.solve_right(B)
            a = self.A.zero()
            for exp, coeff in enumerate(A[:deg]):
                a += coeff * self.A.gen()^exp
            assert self.module_law(a, t1, Omega) == t2
            return (False, a) if return_pol else False
        except ValueError as e:
            if str(e) == 'matrix equation has no solutions':
                return (True, None) if return_pol else True
            else:
                raise e

    def embed(self, Ltau, f):
        """
        Given an Ore polynomial algebra Ltau such that self.Ktau embeds
        in it and an embeding f: K -> L, return a new
        DrinfeldModule_finite_fields with same polynomial ring as self
        and with phi_X defined as the image of self.phi_X in Ltau.
        """
        new_phi_X = f_tau(Ltau, f, self.phi_X)
        return DrinfeldModule_finite_fields(self.A, Ltau, new_phi_X)

    def image(self, a: Polynomial) -> OrePolynomial:
        """
        Given a in A := Fq[X], return phi_a (is in K<tau>).
        """
        # TODO: Check a in Fq[X]
        a = self.__A(a)  # Security cast
        phi_a = self.__Ktau(0)
        for exp, coeff in a.dict().items():
            phi_a += self.__Ktau(coeff) * (self.__phi_X^exp)
        return phi_a

    def lying_polynomial(self, a: Polynomial) -> Polynomial:
        """
        Given a in A := Fq[X], return the polynomial in K[T] lying
        behind phi_a (which is in K<tau>).
        """
        # TODO: Check a in Fq[X]
        T = self.KT.gen()
        phi_a_T = self.KT(0)
        for exp, coeff in self.image(a).dict().items():
            phi_a_T += self.K(coeff) * (T^(self.q^exp))
        return phi_a_T

    def module_law(self, a, omega, Omega=None):
        """
        Given a in A (Fq[X], self.__A here) and l in the algebraic
        closure of K, return `a \star_{\phi} k`, where `\star_\phi` is
        the A-module law on K induced by \phi (self here).
        """
        # TODO: Check a in Fq[X]
        # TODO: Check Omega
        (M, f) = self.__give_algebra(Omega)
        p = self.Fq.characteristic()
        q = self.Fq.order()
        tau_M = M.frobenius_endomorphism(log(q, p))
        # Avoid following error, occuring when constructing several Ore
        # pol. rings with same generator name:
        # None fails to convert into the map's domain Finite Field in a
        # of size 2^16, but a `pushforward` method is not properly
        # implemented.
        letters = string.ascii_letters
        var_name = ''.join(random.choice(letters) for i in range(128))
        Mtau = OrePolynomialRing(M, tau_M, var_name)
        g = lambda x: f_tau(Mtau, f, x)
        # TODO: Check result is in M
        return g(self.image(a))(omega)

    def onedim_torsion_subspace(self, m, V_gen, Omega):
        """
        Given a prime polynomial `m` in self.A and an m-torsion element
        V_gen, return a generator to the sub-A/m-vector space (of the
        m-torsion) generated by V_gen.

        Notice that in this function, Omega is *not* optional.
        """
        # TODO: Verifications
        assert not V_gen.is_zero()
        assert m.is_prime()
        assert self.module_law(m, V_gen, Omega).is_zero()
        L = self.A.quotient_by_principal_ideal(m)
        for abar in L:
            yield self.quotient_law_torsion(m, abar.lift(), V_gen, Omega)

    def quotient_law_torsion(self, m, a, t, Omega=None):
        """
        Given a prime polynomial m in A, an element a of the quotient
        A/<m> and an m-torsion element t in the algebraic closure of K,
        together with an algebra (optional) Omega=(M, f) such that M is
        an algebraic extension of K containing t and f the injection
        from K to L, return (a mod. m)*k, where * is the A/<m>-vector
        space law on the m-torsion M of self, given by:

            A/<m> x M     ->  M
            (a mod. m, t) |-> i_tau(\phi_a)(t)

        This is well defined because t is in M.
        """
        # Verifications
        assert m.is_prime()
        assert self.module_law(m, t, Omega).is_zero()
        # TODO: Check omega in Omega
        # Calculations
        return self.module_law(a, t, Omega)

    def torsion(self, a):
        """
        Given a polynomial a in Fq[X] (self.__A), calculate the a-torsion of
        self. This function returns the tuple (D, f, torsion_gen), where:
        - D is the field extension of K where all the torsion elements live,
        - f is the inclusion K -> D,
        - torsion_gen is a *generator* of the torsion elements.
        """
        # TODO: Verifications
        # 1. Create extension
        phi_a_lying = self.lying_polynomial(a)  # In K[T]
        factors = [factor for (factor, mult) in phi_a_lying.factor()]
        D_order = lcm([factor.degree() for factor in factors])
        D, f = self.K.extension(D_order, map=True)
        # 2. Yield roots
        def torsion_gen():
            for factor in factors:
                for root, mult in factor.roots(D):
                    yield root
        return (D, f, torsion_gen)

    # ─────────── Methods / properties for rank 2 ──────────

    def r2_find_degree_isogeny(self, psi, degree):
        """
        Given another rank two Drinfeld module psi: A -> K{tau}, return an
        isogeny of degree `degree` phi -> psi if there exists one. To do so, we
        implement Joux-Narayan's algorithm and build its related graph and
        return some meta information.

        :return: (found, n. of nodes, n. of nodes per level, isogenies)
        """
        # Verifications
        d = log(self.K.order(), self.Fq.order())
        assert gcd(d, 2) == 1
        assert self.rank == 2
        assert psi.rank == 2
        assert self.phi_X[2] == self.K(1)
        assert psi.phi_X[2] == self.K(1)
        assert psi.Ktau == self.Ktau
        assert psi.A == self.A
        # Actual shit
        g_phi = self.phi_X[1]
        g_psi = psi.phi_X[1]
        omega = self.K.gen()
        q = self.Fq.order()
        isogenies = []
        total_nodes = [1]  # Trick to have a reference to a mutable int
        n_nodes_in_levels = {k: 0 for k in range(degree+1)}
        found = False

        def right_pol_roots(y):
            return (self.X^(q^2) - self.X - y).roots(self.K,
                                                     multiplicities=False)

        def go_deeeep(ukp1_tree, k):
            """
            Calculate q (if possible) nodes of level k given a specific
            lineage (ua, ua-1, ..., uk+1), witha:=degree. A node of
            level n and its associated subtree has the following
            representation:

            {(uk, (ua, ua-1, ..., uk+1): [
                    {(uk+1,1, (ua, ua-1, ..., uk+1, uk): ...},
                    ...
                    {(uk+1,q, (ua, ua-1, ..., uk+1, uk): ...}]}

            To calculate a uk (there are 0 or q possible), one needs to
            know uk+1 and uk+2.
            """
            if k < 0:
                u0, lineage = list(ukp1_tree.keys())[0]
                u1 = lineage[-1]
                if u0*g_phi + u1*(omega^q) == omega*u1 + g_psi*(u0^q):
                    found = True
                    return list(lineage) + [u0]
            else:
                # ukp1_tree has a single key, of the form
                # (uk+1, (1=ua, ua-1, ..., uk+2)), with a = degree
                [key] = list(ukp1_tree.keys())
                ukp1, lineage = key
                ukp2 = lineage[-1]
                right_member = ukp2 * (omega^(q^(k+2)) - omega) \
                               + ukp1 * g_phi^(q^(k+1)) \
                               - g_psi * ukp1^q 
                for r in right_pol_roots(right_member):
                    r_dict = {(r, lineage + (ukp1,)): []}
                    ukp1_tree[key].append(r_dict)
                    total_nodes[0] += 1
                    n_nodes_in_levels[k]+= 1
                    go_deeeep(r_dict, k-1)

        # Build tree
        first_key = (self.K(1), tuple())
        tree = {first_key: []}
        # Lvl degree
        n_nodes_in_levels[degree] = 1
        # Lvl degree-1
        for r in right_pol_roots(g_phi^(q^degree) - g_psi):
            r_dict = {(r, (self.K(1),)): []}
            tree[first_key].append(r_dict)
            total_nodes[0] += 1
            n_nodes_in_levels[degree-1] += 1
            # Lvl degree-3, ..., recursion
            isogenies.append(go_deeeep(r_dict, degree-2))

        return (found, total_nodes[0], n_nodes_in_levels, isogenies)

    def r2_find_isogeny(self, psi, max_degree):
        """
        Find an isogeny from self to psi.
        """
        degree = 3
        found = False
        while not found and degree < max_degree:
            found, _, _, isogenies = self.r2_find_degree_isogeny(psi, degree)
            degree += 1
        if found:
            return isogenies
        else:
            return None

    @property
    def r2_j_invariant(self):
        """
        Return the j-invariant of self, which equals `g^(q+1))/Delta` if
        the image of X is given by `gamma(X) + g*t + Delta*t^2`.
        """
        [gamma, g, Delta] = self.phi_X.coefficients(sparse=False)
        return (g^(self.q + 1)) / Delta

    def r2_torsion_basis(self, m):
        """
        Given a prime polynomial m in a, return a basis of the
        m-torsion; that is, two elements t1 and t2 in the algebraic
        closure of K that are linearly independent. Also return D and f,
        where D is the smallest finite extension of K where both t1 and
        t2 live, and f is the inclusion K -> D.
        """
        # TODO: Verifications
        D, f, torsion_gen = self.torsion(m)
        #             Exactly what we need!
        for t1, t2 in itertools.combinations(torsion_gen(), 2):
            if self.are_independent(m, t1, t2, Omega=(D, f)):
                return (D, f, (t1, t2))

    def r2_torsion_graph_neighbors(self, m):
        """
        Return a list of all j-invariants of rank-two Drinfeld modules
        psi: A -> Ktau such that self and psi are isogenous via a
        separable isogeny whose roots are a one dim. sub-A/m-vector
        space of the m-torsion.

        TODO: Explain return value
        """
        # TODO: Verifications
        gens, Omega = self.all_onedim_torsion_subspaces_gens(m)
        neighbors = set()
        for V_gen in gens:
            P, self_embeded, psi = \
                    self.r2_1dim_torsion_module(m, V_gen, Omega)
            neighbors.add(psi.r2_j_invariant)
        return (Omega, list(neighbors))

    def UNFINISHED_r2_torsion_graph_random_walk(self, m, steps, create_img=False):
        """
        Given a prime pol. m in A, walk randomy in the 'torsion graph'.
        """
        # TODO: Verifications
        graph = dict()
        current_j_dm = self
        for _ in range(steps):
            # Get graph data
            current_j = current_j_dm.r2_j_invariant
            Omega, neighbors = current_j_dm.r2_torsion_graph_neighbors(m)
            if not current_j in graph:
                graph[current_j] = []
            for neighbor in neighbors:
                graph[current_j].append(neighbor)
            # Go randomly to next node
            current_j = random.choice(list(filter(lambda j: j != current_j,
                                             neighbors)))
            D, f = Omega
            Dtau, f_tau_ = embed_Ktau(self.__Ktau, D, f)
            current_j_dm = DrinfeldModule_finite_fields.r2_from_j_invariant(
                    self.__A, Dtau, current_j)
        for j in graph:
            graph[j] = list(set(graph[j]))
        if create_img:
            pretty_graph = dict()
            for j, neighbors in graph.items():
                trunc = 25
                new_key = str(j)[:trunc] + '…'
                pretty_graph[new_key] = []
                for neighbor in neighbors:
                    pretty_graph[new_key].append(str(neighbor)[:trunc] + '…')
            plot = DiGraph(graph)
            plot.graphplot(edge_labels=False, layout='tree').show()
        return graph

    def r2_1dim_torsion_module(self, m, V_gen, Omega):
        # TODO: Verifications
        """
        Given a non-zero m-torsion element V_gen and the extension (D,
        f) where it lives, eturn (P, self_embeded, psi), such that:
        - P: Let V be the one-dim. sub-A/m-vector space of the m-torsion
          generated by V_gen, then P is the additive monic polynomial
          whose roots are the elements of V.
        - self_embeded: phi_embeded is the embedding of self in D{tau}.
        - psi is the unique Drinfeld module psi s.t. P is an isogeny
          from phi to psi_embeded (see paper).
        """
        # TODO: Verifications
        # 1. Create V
        # TODO: Make V a gen. again
        V = list(self.onedim_torsion_subspace(m, V_gen, Omega))
        # 2. Create P (which lives in a bigger ring than Ktau)
        D, f = Omega
        Dtau, f_tau_ = embed_Ktau(self.Ktau, D, f)
        DT.<T_D> = D[]
        P_regpol = reduce(operator.mul, (T_D + v for v in V))
        P = pol_to_ore(self.q, Dtau, P_regpol)
        # 3. Create psi_X
        # 3.1. Prepare stuff
        r = self.rank
        q = self.q
        psi_X_coeffs = [self.phi_X[0]]
        phi_X_coeffs = self.phi_X.coefficients(sparse=False)
        P_coeffs = P.coefficients(sparse=False)

        def s(n, L1, L2, filter_term):
            ind = filter(lambda x: x[0]+x[1] == n,
                         itertools.product(range(n+1), repeat=2))
            if filter_term:
                ind = filter(lambda x: x != (n, 0), ind)
            ind = list(ind)
            prods = (L1[i] * (L2[j]^(q^i)) for (i, j) in ind)
            return reduce(lambda x, y: x+y, prods)

        # 3.2. Calculate psi_n knowing psi_0, …, psi_n-1
        for n in range(1, r+1):
            psi_n = (s(n, P_coeffs, phi_X_coeffs, filter_term=False) \
                    - s(n, psi_X_coeffs, P_coeffs, filter_term=True)) \
                    / P[0]^(q^n)
            psi_X_coeffs.append(psi_n)
        # 3.3. Create psi_X and psi from its calculated coefficients
        psi_X = orecoeffs_to_orepol(q, Dtau, psi_X_coeffs)
        psi = DrinfeldModule_finite_fields(self.A, Dtau, psi_X)
        # 4. Embed self in Dtau
        self_embeded = self.embed(Dtau, f)
        # Return
        return (P, self_embeded, psi)

    # ┌───────┐
    # │ Utils │
    # └───────┘

    def __give_algebra(self, Omega):
        """
        Simple macro.
        """
        return Omega if Omega is not None else (self.K,
                                                self.K.Hom(self.K).identity())

    # ┌────────────┐
    # │ Properties │
    # └────────────┘

    @property
    def A(self) -> PolynomialRing_dense_finite_field:
        return self.__A

    @property
    def Fq(self) -> FiniteField:
        return self.__A.base_ring()

    @property
    def frobenius(self) -> RingHomomorphism:
        return self.__Ktau.twisting_morphism()

    @property
    def K(self) -> FiniteField:
        return self.__Ktau.base_ring()

    @property
    def KT(self):
        return self.__KT

    @property
    def Ktau(self) -> OrePolynomialRing:
        return self.__Ktau

    @property
    def phi_X(self) -> OrePolynomial:
        return self.__phi_X

    @property
    def q(self) -> Integer:
        return self.Fq.order()

    @property
    def rank(self) -> Integer:
        return self.phi_X.degree()

    @property
    def T(self):
        return self.KT.gen()

    @property
    def X(self):
        return self.A.gen()

    # ┌──────────────┐
    # │ Python stuff │
    # └──────────────┘

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        p = f'\
Drinfeld module A -> Ktau, with:\n\
    > Fq:         {self.Fq}\n\
    > A (Fq[X]):  {self.A}\n\
    > K:          {self.K}\n\
    > Ktau:       {self.Ktau}\n\
    > phi_X:      {self.phi_X}'
        return p


# ┌───────┐
# │ Tests │
# └───────┘

# ───────────────── First set of params ────────────────

def test_create_S1():
    p = 2
    q = p^4
    Fq = GF(q)
    K = Fq.extension(2)
    A.<X> = Fq[]
    tau = K.frobenius_endomorphism(log(q, p))
    Ktau.<t> = OrePolynomialRing(K, tau)
    phi_X = 1 + t
    dm = DrinfeldModule_finite_fields(A, Ktau, phi_X)
    return A, X, Ktau, t, dm

def test_S1_are_independent_1():
    A, X, Ktau, t, dm = test_create_S1()
    Fq = A.base_ring()
    K = A.base_ring()
    z4 = Fq.gen()
    z8 = K.gen()
    m = A.irreducible_element(5)
    t1 = K.random_element()
    assert not dm.are_independent(m, t1, 0)
    assert not dm.are_independent(m, 0, t1)

def test_S1_are_independent_2():
    A, X, Ktau, t, dm = test_create_S1()
    Fq = A.base_ring()
    K = A.base_ring()
    z4 = Fq.gen()
    z8 = K.gen()
    m = X^5 + X + z4^3
    t1 = z8^6 + z8^4 + z8^3 + z8^2 + 1
    t2 = z8^5 + z8 + 1
    a = (z4^3 + z4^2 + z4)*X + z4^3 + z4 + 1
    assert not dm.are_independent(m, t1, t2)
    assert not dm.are_independent(m, t2, t1)
    assert dm.module_law(a, t1) == t2

def test_S1_image_1():
    A, X, Ktau, t, dm = test_create_S1()
    a = X^2 + X + 1
    phi_a = 1 + t + t^2
    assert dm.image(a) == phi_a

def test_S1_lying_polynomial_1():
    A, X, Ktau, t, dm = test_create_S1()
    K = Ktau.base_ring()
    q = A.base_ring().order()
    T = dm.KT.gen()
    assert dm.lying_polynomial(K(0)) == K(0)
    assert dm.lying_polynomial(0) == 0
    assert dm.lying_polynomial(1) == T
    assert dm.lying_polynomial(1 + X + X^2) == T + T^q + T^(q^2)

def test_S1_module_law_1():
    A, X, Ktau, t, dm = test_create_S1()
    K = Ktau.base_ring()
    z = K.gen()
    law = dm.module_law
    assert law(0, z) == 0
    assert law(1, z) == z
    assert law(1 + X^2, 0) == 0
    assert law(1 + X + X^2, 1) == 1
    assert law(1 + X + X^2, z) == z^2 + z^3 + z^6

def test_S1_quotient_law_torsion_1():
    A, X, Ktau, t, dm = test_create_S1()
    K = Ktau.base_ring()
    z = K.gen()
    m = X^2 + X + 1
    a = X^3 + 1
    k = z^2 + z

# ────────────── Second set of params (S2) ─────────────

def test_create_S2():
    p = 3
    q = p
    Fq = GF(q)
    K = Fq.extension(2)
    A.<X> = Fq[]
    tau = K.frobenius_endomorphism(log(q, p))
    Ktau.<t> = OrePolynomialRing(K, tau)
    phi_X = t + 2*t^2
    dm = DrinfeldModule_finite_fields(A, Ktau, phi_X)
    return A, X, Ktau, t, dm

def test_S2_quotient_law_torsion_1():
    A, X, Ktau, t, dm = test_create_S2()
    z = Ktau.base_ring().gen()
    m = X^3 + 2*X + 1
    a = X^4 + X^3 + 2*X + 1
    D, f, torsion_gen = dm.torsion(m)
    torsion_slice = itertools.islice(torsion_gen(), 10)
    h = lambda P, t: dm.quotient_law_torsion(m, P, t, (D, f))
    for t in torsion_slice:
        assert h(a, t) == h(a + m, t)

def test_S2_r2_torsion_basis_1():
    A, X, Ktau, t, dm = test_create_S2()
    m = X^2 + 2*X + 2
    D, f, (t1, t2) = dm.r2_torsion_basis(m)
    assert dm.module_law(m, t1, (D, f)) == 0
    assert dm.module_law(m, t2, (D, f)) == 0
    assert dm.are_independent(m, t1, t2, (D, f)) == True

def test_S2_torsion_1():
    A, X, Ktau, t, dm = test_create_S2()
    m = X^2 + 2*X + 2
    D, f, torsion_gen = dm.torsion(m)
    torsion_slice = itertools.islice(torsion_gen(), 10)
    for t in torsion_slice:
        assert dm.module_law(m, t, (D, f)) == 0

# ────────────── Third test of params (S3) ─────────────

def test_create_S3():
    p = 3
    q = p
    Fq = GF(q)
    A.<X> = Fq[]
    K = Fq.extension(2)
    tau = K.frobenius_endomorphism(log(q, p))
    Ktau.<t> = OrePolynomialRing(K, tau)
    phi_X = K.gen() + t^2
    dm = DrinfeldModule_finite_fields(A, Ktau, phi_X)
    return A, X, Ktau, t, dm
    # Pour m, prendre ça, car premier avec la caractéristique :
    # m = X^2 + 1

def test_S3_r2_1dim_torsion_module_1():
    A, X, Ktau, t, dm = test_create_S3()
    m = X^2 + 1
    D, f, torsion_gen = dm.torsion(m)
    V_gen = next(filter(lambda x: x != 0, torsion_gen()))
    P, phi_embeded, psi = dm.r2_1dim_torsion_module(m, V_gen, D, f)
    phi_X = phi_embeded.phi_X
    psi_X = psi.phi_X
    assert P*phi_X == psi_X*P


# ───────────── Fourth t est of params (S4) ────────────

def test_create_S4():
    p = 3
    q = p
    Fq = GF(q)
    A.<X> = Fq[]
    K = Fq.extension(3)
    tau = K.frobenius_endomorphism(log(q, p))
    Ktau.<t> = OrePolynomialRing(K, tau)
    g = K.gen()^4 + 1
    phi_X = K.gen() + g*t + t^2
    dm = DrinfeldModule_finite_fields(A, Ktau, phi_X)
    return A, X, Ktau, t, dm

# ────────────────────── Run tests ─────────────────────

def run_tests():
    # S1
    test_S1_are_independent_1()
    test_S1_are_independent_2()
    test_S1_image_1()
    test_S1_lying_polynomial_1()
    test_S1_module_law_1()
    # S2
    test_S2_torsion_1()
    test_S2_r2_torsion_basis_1()
    test_S2_quotient_law_torsion_1()
    # S3
    test_S3_r2_1dim_torsion_module_1()
