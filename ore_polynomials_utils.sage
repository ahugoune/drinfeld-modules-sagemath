def embed_Ktau(Ktau, D, f):
    """
    Given a field extension K --f--> D and an Ore polynomial ring Ktau
    on K, return the Ore polynomial ring Dtau and the function f_tau_
    such that the following diagram commutes:
        K    --f     --> D
        ↓                ↓
        Ktau --f_tau_--> Dtau
    """
    K_frob = Ktau.twisting_morphism()
    D_frob = D.frobenius_endomorphism(K_frob.power())
    # Avoid following error, occuring when constructing several Ore
    # pol. rings with same generator name:
    # None fails to convert into the map's domain Finite Field in a
    # of size 2^16, but a `pushforward` method is not properly
    # implemented.
    letters = string.ascii_letters
    var_name = ''.join(random.choice(letters) for i in range(128))
    Dtau = OrePolynomialRing(D, D_frob, var_name)
    return (Dtau, lambda P: f_tau(Dtau, f, P))

def f_tau(R2tau, f, P):
    # TODO: Type me
    """
    Given two Ore polynomials algebras R1tau (R1<tau1>) and R2ta
    (R2<tau2>), a ring morphism f: R1 -> R2 and a linear polynomial P in
    R1tau, return the linear polynomial f_tau(P), which is in R2tau. In
    practice, it is useless to require R1tau as an argument, so we do
    not.
    """
    # TODO: Check well definition of args
    P_tau = R2tau(0)
    for exp, coeff in P.dict().items():
        P_tau += f(coeff) * (R2tau.gen()^exp)
    return P_tau

def orecoeffs_to_orepol(q, Ktau, Ptau_coeffs):
    # TODO: Type me
    """
    Given a list representation (containing coefficients) of a linear
    polynomial, return this linear polynomial.
    """
    # TODO: Verifications
    tau = Ktau.gen()
    Ptau = Ktau(0)  # P(tau)
    for exp, coeff in enumerate(Ptau_coeffs):
        if coeff != 0:
            Ptau += coeff * (tau^exp)
    return Ptau

def pol_to_ore(q, Ktau, P):
    # TODO: Type me
    """
    Given an Ore polynomial algebra Ktau (of the form K{tau}, tau being
    the q-power Frob. endomorphism of K) and a polynomial P in K[X],
    return an element P(tau) if P is in K{tau}, return None otherwise.
    """
    # TODO: Verifications
    tau = Ktau.gen()
    Ptau = Ktau(0)  # P(tau)
    for exp, coeff in P.dict().items():
        i = log(exp, q)
        if not i in NN:  # If exp is not of the form q^int
            if coeff != 0:
                return None
        else:
            Ptau += coeff * (tau^i)
    return Ptau

# ┌───────┐
# │ Tests │
# └───────┘

def test_f_tau_1():
    R1 = GF(7^3)
    (R2, f) = R1.extension(4, map=True)
    tau1 = R1.frobenius_endomorphism(2)
    tau2 = R2.frobenius_endomorphism(2)
    R1tau.<t1> = OrePolynomialRing(R1, tau1)
    R2tau.<t2> = OrePolynomialRing(R2, tau2)
    P = 1 + t1 + 6*t1^4
    assert f_tau(R2tau, f, P) == 1 + t2 + 6*t2^4

def test_pol_to_ore_1():
    p = 2
    q = p^2
    K = GF(q^2)
    A.<X> = K[]
    frob = K.frobenius_endomorphism(log(q, p))
    Ktau.<tau> = OrePolynomialRing(K, frob)
    P1 = X^q + X
    assert pol_to_ore(q, Ktau, P1) == 1 + tau
    P2 = X^(q^2) + X^3
    assert pol_to_ore(q, Ktau, P2) == None
    P3 = X^(q^5) + 1
    assert pol_to_ore(q, Ktau, P3) == None
    P4 = X^(q^5) + X^(q^3) + X
    assert pol_to_ore(q, Ktau, P4) == tau^5 + tau^3 + 1

def run_tests():
    test_f_tau_1()
    test_pol_to_ore_1()
