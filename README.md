# SageMath implementation of Drinfeld modules over finite fields

This repository hosts a basic SageMath implementation of Drinfeld modules over
finite fields.

No documentation (except for the classes and functions'
[docstrings](https://www.python.org/dev/peps/pep-0257/#what-is-a-docstring) is
provided here as this repository is temporary (we aim at directly contributing
to SageMath).

See file [demo.sage](demo.sage) for a demo of the code.

# Author

Antoine Leudière, Intern at [INRIA CARAMBA](https://caramba.loria.fr/).

# License

This project is licensed under the terms of the GNU General Public License v3.0.
See the [LICENSE](LICENSE) file for more information.
